package github.timguy.stillskill;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import github.timguy.stillskill.helper.GermanSpeechableTimeHelper;

public class StillSkillOfflineTest {
	
	@Test
	public void testGermanSpeechableTimeHelper() throws ParseException, IOException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		Date savedDate = dateFormat.parse("2017.08.15 10:00:00");
		
		Date dateLater = dateFormat.parse("2017.08.15 12:35:00");
		assertEquals("Vor 2 Stunden und 35 Minuten", GermanSpeechableTimeHelper.speechableTimeDifference(savedDate, dateLater));
		
		dateLater = dateFormat.parse("2017.08.15 11:00:00");
		assertEquals("Vor einer Stunde", GermanSpeechableTimeHelper.speechableTimeDifference(savedDate, dateLater));
		
		dateLater = dateFormat.parse("2017.08.15 13:00:00");
		assertEquals("Vor 3 Stunden", GermanSpeechableTimeHelper.speechableTimeDifference(savedDate, dateLater));
		
		dateLater = dateFormat.parse("2017.08.15 13:01:00");
		assertEquals("Vor 3 Stunden und einer Minute", GermanSpeechableTimeHelper.speechableTimeDifference(savedDate, dateLater));
		
		dateLater = dateFormat.parse("2017.08.16 09:59:00");
		assertEquals("Vor 23 Stunden und 59 Minuten", GermanSpeechableTimeHelper.speechableTimeDifference(savedDate, dateLater));
		
		dateLater = dateFormat.parse("2017.08.16 10:00:00");
		assertEquals("Am Dienstag den 15. August um 11:00", GermanSpeechableTimeHelper.speechableTimeDifference(savedDate, dateLater));
		
		dateLater = dateFormat.parse("2018.01.19 10:00:00");
		assertEquals("Am Dienstag den 15. August um 11:00", GermanSpeechableTimeHelper.speechableTimeDifference(savedDate, dateLater));
		
	}

}
