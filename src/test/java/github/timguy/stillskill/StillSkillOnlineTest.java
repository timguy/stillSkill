package github.timguy.stillskill;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Ignore;
import org.junit.Test;

import github.timguy.stillskill.helper.GermanSpeechableTimeHelper;
import github.timguy.stillskill.helper.NoStillzeitFoundException;
import github.timguy.stillskill.persistance.DynamoDBService;
import github.timguy.stillskill.persistance.StillzeitEntity;

public class StillSkillOnlineTest {
	
	@Test
	public void testSingleDbEntry()
			throws ParseException, IOException, NoStillzeitFoundException {
		String userId = "Junit-testSingleDbEntry";
		DynamoDBService dynamo = new DynamoDBService();
		dynamo.deleteByUserId(userId);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss" , Locale.UK);
		Date savedDate = dateFormat.parse("2017.08.15 10:00:00");
		
		DateUtils.addDays(new Date(), -1);
		
		StillzeitEntity entity = new StillzeitEntity(userId, savedDate, true);
		dynamo.save(entity);

		List<StillzeitEntity> list = dynamo.queryByUserIdNewestFirst(userId);  //dynamo.findByOwnerQuery(userId);
		assertThat(list, hasSize(1));
		StillzeitEntity stillzeitEntity = list.get(0);
		
		Date eventDate = stillzeitEntity.getEventDate();
		System.out.println("Direct from DB: " + eventDate);
		assertEquals("Am Dienstag den 15. August um 11:00", GermanSpeechableTimeHelper.speechableTimeDifference(eventDate, new Date()));
		
		
		//dynamo.delete(stillzeitEntity);
	}
	
	@Test
	@Ignore //only needed when we need to delete massive entries in production
	public void delete() throws ParseException, IOException, NoStillzeitFoundException {
		String userId = "Junit-testMultipleEntries";
		DynamoDBService dynamo = new DynamoDBService();
		dynamo.deleteByUserId(userId);
	}
	
	@Test
	public void testMultipleEntries() throws ParseException, IOException, NoStillzeitFoundException {
		String userId = "Junit-testMultipleEntries";
		DynamoDBService dynamo = new DynamoDBService();
		dynamo.deleteByUserId(userId);
		
		for(int i=0; i < 10; i++)
		{
			StillzeitEntity entity = new StillzeitEntity(userId, new Date(), (i < 5 ? true: false));
			dynamo.save(entity);
		}
		
		List<StillzeitEntity> list = dynamo.queryByUserIdNewestFirst(userId);
		assertThat(list, hasSize(10));
		
		long countRightSide = list.stream().filter(t -> t.isRightSide() == true).count();
		assertThat(Math.toIntExact(countRightSide), equalTo(5));
	}
	
}
