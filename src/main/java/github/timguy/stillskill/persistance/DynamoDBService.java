package github.timguy.stillskill.persistance;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalOperator;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.xspec.QueryExpressionSpec;
import com.amazonaws.services.lambda.runtime.Context;

import github.timguy.stillskill.helper.NoStillzeitFoundException;

public class DynamoDBService {
	private static AmazonDynamoDBClient dynamoDBclient;
	private static final org.slf4j.Logger log = LoggerFactory
			.getLogger(AmazonDynamoDBClient.class);
	private DynamoDBMapper mapper;

	public DynamoDBService() {
		log.debug("start create dynamoDb Client");
		dynamoDBclient = new AmazonDynamoDBClient().withRegion(Region
				.getRegion(Regions.EU_WEST_1));
		log.debug("dynamoDb Client created");
		// UpdateItemResult updateItemResult = persistSingleValues();
		mapper = new DynamoDBMapper(dynamoDBclient);
		log.debug("Mapper ready.");
	}

	public StillzeitEntity queryLastEntry(String owner)
			throws NoStillzeitFoundException {
		log.debug("Find lastStillzeit for: " + owner);

		List<StillzeitEntity> latestStillzeit = queryByUserIdNewestFirst(owner);
		// assume last entry in list was entered latest in dynamo db
		return latestStillzeit.get(0);
	}

	public List<StillzeitEntity> queryByUserIdNewestFirst(String userId)
			throws NoStillzeitFoundException {
		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS(userId));

		DynamoDBQueryExpression<StillzeitEntity> queryExpression = new DynamoDBQueryExpression<StillzeitEntity>()
				.withKeyConditionExpression("userId = :val1")
				.withExpressionAttributeValues(eav);

		List<StillzeitEntity> list = mapper.query(StillzeitEntity.class,
				queryExpression);
		if (list.isEmpty()) {
			throw new NoStillzeitFoundException();
		}

		List<StillzeitEntity> reverseList = new ArrayList<>(list);
		Collections.reverse(reverseList);
		return reverseList;
	}

	public List<StillzeitEntity> findStillzeitLastWeek(DynamoDBMapper mapper,
			String owner)
			throws Exception {
		System.out.println("FindRepliesInLast7Days: Stillzeit last 7 days.");

		long oneWeekAgoMillis = (new Date()).getTime()
				- (7L * 24L * 60L * 60L * 1000L);
		Date oneWeekAgo = new Date();
		oneWeekAgo.setTime(oneWeekAgoMillis);
		SimpleDateFormat dateFormatter = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		String oneWeeksAgoStr = dateFormatter.format(oneWeekAgo);

		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS(owner));
		eav.put(":val2", new AttributeValue().withS(oneWeeksAgoStr.toString()));

		DynamoDBQueryExpression<StillzeitEntity> queryExpression = new DynamoDBQueryExpression<StillzeitEntity>()
				.withKeyConditionExpression(
						"userId = :val1 and eventDate > :val2")
				.withExpressionAttributeValues(eav);

		List<StillzeitEntity> latestStillzeit = mapper
				.query(StillzeitEntity.class, queryExpression);

		for (StillzeitEntity stillzeitEntity : latestStillzeit) {
			log.debug(stillzeitEntity.toString());
		}
		return latestStillzeit;
	}

	public void save(StillzeitEntity stillzeitEntity) {
		log.debug("speichere...:");
		mapper.save(stillzeitEntity);
		log.debug("...fertig");
	}

	public void delete(StillzeitEntity stillzeitEntity) {
		mapper.delete(stillzeitEntity);
	}

	public int deleteByUserId(String userId) throws NoStillzeitFoundException {
		List<StillzeitEntity> list = null;
		int countDeleted = 0;
		list = queryByUserIdNewestFirst(userId);
		countDeleted = list.size();
		for (StillzeitEntity s : list) {
			delete(s);
		}
		return countDeleted;
	}
}
