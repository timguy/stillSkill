package github.timguy.stillskill.helper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.LoggerFactory;

public class GermanSpeechableTimeHelper {

	// Dienstag den 23. Mai um 18:00
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE 'den' dd. MMMM 'um' HH:mm", Locale.GERMANY);

	private static final org.slf4j.Logger log = LoggerFactory
			.getLogger(GermanSpeechableTimeHelper.class);
	/**
	 * Makes a date difference to a speechable text. Until 23 hours in hour and minute resolution.
	 * Otherwise just the oldDate ist named
	 * @param oldDate
	 * @param now
	 * @return
	 */
	public static String speechableTimeDifference(Date oldDate, Date now) {
		long diff = now.getTime() - oldDate.getTime(); 
		long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);
		long minutes = TimeUnit.MILLISECONDS.toMinutes(diff); 
		long hours = TimeUnit.MILLISECONDS.toHours(diff); 
		long days = TimeUnit.MILLISECONDS.toDays(diff);
		log.info("Sec: " + seconds + "    Minutes: " + minutes + "    hours: " + hours + "    days: " + days);
		
		String prefix = "Vor ";
		if(minutes < 60) {
			return prefix + minutes + " Minuten";
		}else if(hours < 24) {
			long minutesPart = minutes - (hours * 60);
			if(minutesPart > 0) {
				return prefix + hoursSingularPlural(hours) + " und " + minutesSingularPlural(minutesPart);
			}else {
				return prefix + hoursSingularPlural(hours);
			}
		}
		Date oldDateGerman = DateUtils.addHours(oldDate, +1);
		return "Am " + simpleDateFormat.format(oldDateGerman);
	}
	
	private static String hoursSingularPlural(long hours) {
		if (hours == 1) {
			return "einer Stunde";
		} else {
			return hours + " Stunden";
		}
	}
	
	private static String minutesSingularPlural(long minutes) {
		if (minutes == 1) {
			return "einer Minute";
		} else {
			return minutes + " Minuten";
		}
	}
}
