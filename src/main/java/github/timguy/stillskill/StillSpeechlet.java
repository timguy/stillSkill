package github.timguy.stillskill;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazon.speech.json.SpeechletRequestEnvelope;
import com.amazon.speech.slu.ConfirmationStatus;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.slu.entityresolution.Resolution;
import com.amazon.speech.slu.entityresolution.Resolutions;
import com.amazon.speech.slu.entityresolution.StatusCode;
import com.amazon.speech.slu.entityresolution.ValueWrapper;
import com.amazon.speech.speechlet.Directive;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.IntentRequest.DialogState;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.speechlet.SpeechletV2;
import com.amazon.speech.speechlet.User;
import com.amazon.speech.speechlet.dialog.directives.DelegateDirective;
import com.amazon.speech.speechlet.dialog.directives.DialogIntent;
import com.amazon.speech.speechlet.dialog.directives.ElicitSlotDirective;
import com.amazon.speech.ui.OutputSpeech;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;
import com.amazon.speech.ui.SimpleCard;

import github.timguy.stillskill.helper.AlexaSpeechletResponse;
import github.timguy.stillskill.helper.NoStillzeitFoundException;
import github.timguy.stillskill.persistance.DynamoDBService;
import github.timguy.stillskill.persistance.StillzeitEntity;

public class StillSpeechlet implements SpeechletV2 {

	private static final DynamoDBService dynamoDBService = new DynamoDBService();

	private static final String SSML_BREAK = "<break time=\"0.5s\" />";
	private static final Logger log = LoggerFactory
			.getLogger(StillSpeechlet.class);

	private static final String SLOT_SIDE = "sideSlot";

	@Override
	public void onSessionStarted(
			SpeechletRequestEnvelope<SessionStartedRequest> requestEnvelope) {
		log.info("onSessionStarted requestId={}, sessionId={}",
				requestEnvelope.getRequest().getRequestId(),
				requestEnvelope.getSession().getSessionId());
	}

	@Override
	public SpeechletResponse onLaunch(
			SpeechletRequestEnvelope<LaunchRequest> requestEnvelope) {
		log.info("onLaunch requestId={}, sessionId={}",
				requestEnvelope.getRequest().getRequestId(),
				requestEnvelope.getSession().getSessionId());
		return handleHelp("");
	}

	@Override
	public SpeechletResponse onIntent(
			SpeechletRequestEnvelope<IntentRequest> requestEnvelope) {

		String userId = requestEnvelope.getSession().getUser().getUserId();

		Intent intent = requestEnvelope.getRequest().getIntent();

		log.info("onIntent requestId={}, sessionId={}, intentName={}",
				requestEnvelope.getRequest().getRequestId(),
				requestEnvelope.getSession().getSessionId(), intent.getName());

		switch (intent.getName()) {
			case "EntryIntent" :
				return save(intent, userId, requestEnvelope);
			case "LastIntent" :
				return load(userId);
			case "HistoryIntent" :
				return historie(userId);
			case "DeleteAllIntent" :
				return deleteAll(userId, requestEnvelope);
			case "AMAZON.HelpIntent" :
				return handleHelp("");
			case "AMAZON.StopIntent" :
				return AlexaSpeechletResponse.tell()
						.withText("Na denn. Tschüß.").build();
			case "AMAZON.CancelIntent" :
				return AlexaSpeechletResponse.tell().withText("Bis bald.")
						.build();
			default : {
				log.error("Unknown intent: " + intent.getName());
				return AlexaSpeechletResponse.tell()
						.withText("Ich habe deine Absicht nicht verstanden")
						.build();
			}
		}
	}

	private SpeechletResponse load(String userId) {
		try {
			StillzeitEntity lastStillzeit = dynamoDBService
					.queryLastEntry(userId);

			return AlexaSpeechletResponse.tell() //
					.withSsml("Deine letzte Stillzeit war "
							+ lastStillzeit.toStringAlexaSpeech() + SSML_BREAK
							+ "Tschüss.") //
					.withSimpleCardTitle("Letzte Stillzeit")
					.withSimpleCardContent(lastStillzeit.toStringAlexaSpeech())
					.build();
		} catch (NoStillzeitFoundException e) {
			return responseNoStillzeitYet();
		}
	}

	private AlexaSpeechletResponse responseNoStillzeitYet() {
		return AlexaSpeechletResponse.tell() //
				.withText("Du hast noch keine Zeiten eingetragen. Tschüß.") //
				.build();
	}

	private SpeechletResponse deleteAll(String userId,
			SpeechletRequestEnvelope<IntentRequest> requestEnvelope) {

		if (DialogState.COMPLETED
				.equals(requestEnvelope.getRequest().getDialogState())) {

			log.info("DialogueState for delete: Completed");

			int countDeletedEntries;
			try {
				countDeletedEntries = dynamoDBService
						.deleteByUserId(userId);
			} catch (NoStillzeitFoundException e) {
				return responseNoStillzeitYet();
			}

			return AlexaSpeechletResponse.tell() //
					.withSsml("Alle " + countDeletedEntries
							+ " Einträge gelöscht" + "Tschüss.") //
					.build();
		} else {
			return respondDialogeNotCompleted();
		}
	}
	
	private SpeechletResponse historie(String userId) {
		try {
			List<StillzeitEntity> stillzeiten = dynamoDBService
					.queryByUserIdNewestFirst(userId);

			StringBuilder sbSpeech = new StringBuilder(
					"Deine Historie mit " + stillzeiten.size()
							+ " Einträgen. Sage Alexa Stop um abzubrechen. ");
			StringBuilder sbCard = new StringBuilder(sbSpeech);
			for (StillzeitEntity stillzeit : stillzeiten) {
				String entityString = stillzeit.toStringAlexaSpeech();
				sbSpeech.append(entityString);
				sbCard.append(entityString);
				
				sbSpeech.append(SSML_BREAK);
				sbCard.append("\n");
			}
			return AlexaSpeechletResponse.tell() //
					.withSsml(sbSpeech.toString() + "Tschüss.") //
					.withSimpleCardTitle("Historie")
					.withSimpleCardContent(sbCard.toString())
					.build();
		} catch (NoStillzeitFoundException e) {
			return responseNoStillzeitYet();
		}
	}

	private SpeechletResponse handleHelp(String prefixErrorSpeechmessage) {
		// Create the plain text output.
		String speechOutput = (prefixErrorSpeechmessage==null ? "" : prefixErrorSpeechmessage)
				+ "Mit Stillzeiten kannst du deine Stillzeiten eintragen oder den letzten Eintrag abrufen. Sage zum Beispiel Historie "
				+ SSML_BREAK + " oder Letzer Eintrag " + SSML_BREAK
				+ " oder Neuer Eintrag auf linker Seite gestillt";

		String repromptText = "Was nun? Neuer Eintrag oder Letzter Eintrag.";

		return AlexaSpeechletResponse.ask().withRepromptText(repromptText)
				.withSsml(speechOutput)
				.build();
	}

	private SpeechletResponse save(Intent intent, String userId,
			SpeechletRequestEnvelope<IntentRequest> requestEnvelope) {

		if (DialogState.COMPLETED
				.equals(requestEnvelope.getRequest().getDialogState())) {
			log.info("DialogueState: Completed");
			StillzeitEntity entity = new StillzeitEntity();

			try {
				entity = getSide(intent, entity);
			} catch (IOException e) {
				return respondElicitSideSlot();
			}
			entity.setEventDate(requestEnvelope.getRequest().getTimestamp());
			entity.setUserId(userId);
			dynamoDBService.save(entity);

			return AlexaSpeechletResponse.tell()
					.withSsml("Stillzeit gespeichert. Mit Seite "
							+ entity.getSide() + ". Tschau.")
					.withSimpleCardTitle("Stillzeit gespeichert")
					.withSimpleCardContent("Seite: " + entity.getSide())
					.build();
		} else {
			log.info("DialogueState: Not Completed");
			return respondDialogeNotCompleted();
		}
	}

	private SpeechletResponse respondDialogeNotCompleted() {
		SpeechletResponse speechletResponse = new SpeechletResponse();
		DelegateDirective delegateDirective = new DelegateDirective();
		// https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/dialog-interface-reference#directives
		speechletResponse.setDirectives(
				Arrays.asList(new Directive[]{delegateDirective}));
		speechletResponse.setNullableShouldEndSession(false);
		return speechletResponse;
	}
	
	private SpeechletResponse respondElicitSideSlot() {
		log.error("Respond Elicit");
		SpeechletResponse speechletResponse = new SpeechletResponse();
		ElicitSlotDirective elicitDirective = new ElicitSlotDirective();
		// https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/dialog-interface-reference#directives
		elicitDirective.setSlotToElicit(SLOT_SIDE);
		speechletResponse.setDirectives(
				Arrays.asList(new Directive[]{elicitDirective}));
		speechletResponse.setNullableShouldEndSession(false);
		final PlainTextOutputSpeech plainSpeech = new PlainTextOutputSpeech();
		plainSpeech.setText("Auf welcher Seite hast du gestillt?");
		Reprompt reprompt = new Reprompt();
		reprompt.setOutputSpeech(plainSpeech);
		speechletResponse.setReprompt(reprompt);
		
		speechletResponse.setOutputSpeech(plainSpeech);
		return speechletResponse;
	}

	private StillzeitEntity getSide(Intent intent, StillzeitEntity entity) throws IOException {
		Slot slotSide = intent.getSlot(SLOT_SIDE);
		if (slotSide == null) {
			String errorMsg = "No sideSlot found. Due to dialogue model and collecting side value this should never happen.";
			log.error(errorMsg);
			throw new IOException(errorMsg);
		}
		Resolutions resolutions = slotSide.getResolutions();
		log.info(resolutions.toString());
		List<Resolution> resolutionsPerAuthority = resolutions
				.getResolutionsPerAuthority();
		if (resolutionsPerAuthority.size() > 0) {
			Resolution resolution = resolutionsPerAuthority.get(0);
			log.info("Resolution Status: " + resolution.getStatus().getCode().toString());
			
			if(StatusCode.ER_SUCCESS_MATCH != resolution.getStatus().getCode())
			{
				String errorMsg = "No success in Entity Resolution. No synonym or rechts links found";
				log.error(errorMsg);
				throw new IOException(errorMsg);
			}
			
			List<ValueWrapper> valueWrappers = resolution.getValueWrappers();
			if (valueWrappers.size() > 0) {
				ValueWrapper valueWrapper = valueWrappers.get(0);
				String name = valueWrapper.getValue().getName();
				log.info("Recognized in dialogue: slotSide.Value: " + slotSide.getValue() + " valueWrapperName: " + name);
				if (name.equals("rechts")) {
					entity.setRightSide(true);
				} else {
					if(name.equals("links")) {
						entity.setRightSide(false);
					}else {
						String message = "No side detected! Should not happend due to Entity Resolution status check";
						log.warn(message);
						throw new IOException(message);
					}
				}
			}
		}
		return entity;
	}

	@Override
	public void onSessionEnded(
			SpeechletRequestEnvelope<SessionEndedRequest> requestEnvelope) {
		log.info("onSessionEnded requestId={}, sessionId={}",
				requestEnvelope.getRequest().getRequestId(),
				requestEnvelope.getSession().getSessionId());
	}

}
